using Essentials;

namespace Environment
{
	public class EnvironmentObject
	{
		private Texture texture;
		
		protected float x;
		protected float y;

		protected float size;

		private RectangleFloat rectangle;

		private Texture Texture {
			get {
				return texture;
			}
			set {
				texture = value;
			}
		}

		public float X {
			get {
				return x;
			}
			set {
				x = value;

				Rectangle.X1 = x;
				Rectangle.X2 = x + Size;
			}
		}

		public float Y {
			get {
				return y;
			}
			set {
				y = value;

				Rectangle.Y1 = y;
				Rectangle.Y2 = y + Size;
			}
		}

		public float Size {
			get {
				return size;
			}
			set {
				size = value;
			}
		}

		public RectangleFloat Rectangle {
			get {
				return rectangle;
			}
			protected set {
				rectangle = value;
			}
		}

		public EnvironmentObject(string filename, float x, float y, float size)
		{
			Rectangle = new RectangleFloat(x, y, x + size, y + size);
			
			this.Texture = new Texture(filename);

			Size = size;

			X = x;
			Y = y;
		}

		public void Render()
		{
			Texture.RenderWidthHeight(X, Y, Size, Size);
		}
	}
}
