using System;
using OpenTK;
using Essentials;

namespace Characters
{
	public class DonQuixote : Swordsman
	{
		private const string BODY_FILE = "res/units/swordsman/body.png";
		private const string ARM_FILE = "res/units/swordsman/arm.png";
		private const string HELMET_FILE = "res/units/swordsman/helmet.png";
		private const string SWORD_FILE = "res/units/swordsman/sword.png";

		private RectangleFloat swordRectangle;

		public RectangleFloat SwordRectangle {
			get {
				return swordRectangle;
			}
			private set {
				swordRectangle = value;
			}
		}

		private float SwordX1 {
			get {
				if (Rotation == 0.0f)
					return X + 0.055f;
				else if (Rotation == 90.0f)
					return X + 0.07f;
				else if (Rotation == 180.0f)
					return X - 0.055f;
				else
					return X - 0.07f;
			}
		}

		private float SwordX2 {
			get {
				if (Rotation == 0.0f)
					return SwordX1 + 0.1f;
				else if (Rotation == 90.0f)
					return SwordX1 + 0.04f;
				else if (Rotation == 180.0f)
					return SwordX1 + 0.1f;
				else
					return SwordX1 + 0.04f;
			}
		}

		private float SwordY1 {
			get {
				if (Rotation == 0.0f)
					return Y - 0.04f;
				else if (Rotation == 90.0f)
					return Y + 0.085f;
				else if (Rotation == 180.0f)
					return Y + 0.1f;
				else
					return Y - 0.025f;
			}
		}

		private float SwordY2 {
			get {
				if (Rotation == 0.0f)
					return SwordY1 + 0.04f;
				else if (Rotation == 90.0f)
					return SwordY1 + 0.1f;
				else if (Rotation == 180.0f)
					return SwordY1 + 0.04f;
				else
					return SwordY1 + 0.1f;
			}
		}
		
		public DonQuixote() : base()
		{
			Textures.Add(new Texture(BODY_FILE));
			Textures.Add(new Texture(ARM_FILE));
			Textures.Add(new Texture(HELMET_FILE));
			Textures.Add(new Texture(SWORD_FILE));

			SwordRectangle = new RectangleFloat(SwordX1, SwordY1, SwordX2, SwordY2);
		}

		public override void Render()
		{
			var cos = (float)Math.Cos(MathHelper.DegreesToRadians(Rotation));
			var sin = (float)Math.Sin(MathHelper.DegreesToRadians(Rotation));
			
			TextureForFilename(HELMET_FILE).RenderWidthHeight(X + 0.025f + (cos * 0.05f), Y + 0.01f + (sin * 0.05f), 0.05f, 0.08f);
			
			TextureForFilename(BODY_FILE).RenderWidthHeight(X, Y, 0.1f, 0.1f);

			cos = (float)Math.Cos(MathHelper.DegreesToRadians(Rotation + 90.0f));
			sin = (float)Math.Sin(MathHelper.DegreesToRadians(Rotation + 90.0f));

			TextureForFilename(ARM_FILE).RenderWidthHeight(X + 0.025f + (cos * 0.07f), Y + 0.025f + (sin * 0.07f), 0.05f, 0.05f);

			cos = (float)Math.Cos(MathHelper.DegreesToRadians(Rotation + 270.0f));
			sin = (float)Math.Sin(MathHelper.DegreesToRadians(Rotation + 270.0f));

			// BAD
			// Update sword rectangle.
			SwordRectangle.X1 = SwordX1;
			SwordRectangle.Y1 = SwordY1;
			SwordRectangle.X2 = SwordX2;
			SwordRectangle.Y2 = SwordY2;
			
			TextureForFilename(SWORD_FILE).RenderWidthHeight(SwordX1, SwordY1, 0.1f, 0.04f);
			
			TextureForFilename(ARM_FILE).RenderWidthHeight(X + 0.025f + (cos * 0.07f), Y + 0.025f + (sin * 0.07f), 0.05f, 0.05f);
		}
	}
}
