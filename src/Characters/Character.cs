using System;
using System.Drawing;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;
using Essentials;

namespace Characters
{
	public class Character
	{
		protected const float SIZE = 0.1f;
		
		private List<Texture> textures;

		private float x = -(SIZE / 2.0f);
		private float y = -(SIZE / 2.0f);

		private float rotation;

		private RectangleFloat rectangle;

		public List<Texture> Textures {
			get {
				return textures;
			}
			private set {
				textures = value;
			}
		}

		public float X {
			get {
				return x;
			}
			set {
				x = value;

				Rectangle.X1 = x;
				Rectangle.X2 = x + SIZE;
			}
		}

		public float Y {
			get {
				return y;
			}
			set {
				y = value;

				Rectangle.Y1 = y;
				Rectangle.Y2 = y + SIZE;
			}
		}

		public float Rotation {
			get {
				return rotation;
			}
			set {
				if (value >= 0.0f && value < 360.0f)
					rotation = value;
				else
					Console.WriteLine("Invalid rotation angle " + value);

				SetTextureRotations();
			}
		}

		public RectangleFloat Rectangle {
			get {
				return rectangle;
			}
			protected set {
				rectangle = value;
			}
		}

		public Character()
		{
			Textures = new List<Texture>(0);

			Rectangle = new RectangleFloat(X, Y, X + SIZE, Y + SIZE);
		}

		private void SetTextureRotations()
		{
			foreach (Texture texture in Textures)
				texture.Rotation = Rotation;
		}

		protected Texture TextureForFilename(string filename)
		{
			foreach (Texture texture in Textures)
				if (texture.Filename == filename)
					return texture;

			return null;
		}

		public virtual void Render()
		{
			foreach (Texture texture in Textures)
				texture.RenderWidthHeight(X, Y, SIZE, SIZE);
		}
	}
}
