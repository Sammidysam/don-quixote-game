namespace Characters
{
	public class Swordsman : Character
	{
		private bool swordExtended;

		public bool SwordExtended {
			get {
				return swordExtended;
			}
			set {
				swordExtended = value;
			}
		}

		public Swordsman() : base()
		{
			
		}
	}
}
