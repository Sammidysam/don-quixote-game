using System;
using System.Drawing;
using OpenTK;
using OpenTK.Input;
using OpenTK.Graphics.OpenGL;

namespace Essentials
{
	public class Start : GameWindow
	{
		private Drawer drawer;
		private Manager manager;
		private Mode mode;

		private Drawer Drawer {
			get {
				return drawer;
			}
			set {
				drawer = value;
			}
		}

		private Manager Manager {
			get {
				return manager;
			}
			set {
				manager = value;
			}
		}

		private Mode Mode {
			get {
				return mode;
			}
			set {
				mode = value;

				// Clear the renderer.
				Drawer.Renderer.Clear();

				// Add mode-specific text to the renderer.
				switch (mode) {
					case Mode.Menu:
						Drawer.AddMenuStrings(Width, Height);
						break;
					case Mode.Instructions:
						Drawer.AddInstructionsStrings(Width, Height);
						break;
				}
			}
		}
		
		public Start() : base(DisplayDevice.Default.Width, DisplayDevice.Default.Height)
		{
			
		}
		
		protected override void OnLoad(EventArgs e)
		{
			this.Title = "Don Quixote - The Game";
			this.WindowState = WindowState.Fullscreen;
			
			this.Drawer = new Drawer(Width, Height);
			this.Manager = new Manager();
			this.Mode = Mode.Menu;
			
			GL.Enable(EnableCap.Blend);
			GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
		}

		protected override void OnUnload(EventArgs e)
		{
			Drawer.Renderer.Dispose();
		}
		
		protected override void OnResize(EventArgs e)
		{
			GL.Viewport(ClientRectangle);
			GL.MatrixMode(MatrixMode.Projection);
			GL.LoadIdentity();
			GL.Ortho(-1.0, 1.0, -1.0, 1.0, 0.0, 4.0);
		}
		
		protected override void OnUpdateFrame(FrameEventArgs e)
		{
			// Keyboard handling.
			if (Keyboard[Key.Escape])
				this.Exit();
			else if (Keyboard[Key.M])
				Mode = Mode.Menu;

			if (Mode == Mode.Menu) {
				if (Keyboard[Key.Space]) {
					this.Mode = Mode.InGame;
				} else if (Keyboard[Key.I]) {
					this.Mode = Mode.Instructions;
				}
			} else if (Mode == Mode.InGame) {
				Manager.KeyboardHandle(Keyboard);
				Manager.MouseHandle(Mouse, Width, Height);

				// Let Manager handle CPU logic.
				Manager.Logic();

				Drawer.Renderer.Clear();

				var windmillFont = new Font(FontFamily.GenericSerif, 30);
				var point = PointF.Empty;
				var text = "Windmills destroyed: " + Manager.WindmillsDestroyed;

				point.X = Drawer.CenterString(text, windmillFont, Width);
				point.Y = Height * 0.85f;

				Drawer.Renderer.AddText(text, windmillFont, Brushes.White, point);
			}
		}
		
		protected override void OnRenderFrame(FrameEventArgs e)
		{
			Drawer.Render(Mode, Manager);
			
			this.SwapBuffers();
		}
		
		static void Main()
		{
			using (Start start = new Start()) {
				Console.WriteLine("Running...");
				start.Run(20, 20);
			}
		}
	}
}
