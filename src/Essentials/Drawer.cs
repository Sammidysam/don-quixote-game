using System.Drawing;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;
using Environment;

namespace Essentials
{
	public class Drawer
	{
		private const float CLOUD_SIZE = 0.3f;
		
		private TextRenderer renderer;

		private Texture cloud;
		// private List<PointF> cloudPoints;

		public TextRenderer Renderer {
			get {
				return renderer;
			}
			private set {
				renderer = value;
			}
		}

		private Texture Cloud {
			get {
				return cloud;
			}
			set {
				cloud = value;
			}
		}

		// private List<PointF> CloudPoints {
		// 	get {
		// 		return cloudPoints;
		// 	}
		// 	set {
		// 		cloudPoints = value;
		// 	}
		// }
		
		public Drawer(int width, int height)
		{ 
			this.Renderer = new TextRenderer(width, height);
			Renderer.Clear();

			Cloud = new Texture("res/environment/cloud.png");

			// CloudPoints = new List<PointF>(0);
		}

		public void AddMenuStrings(int width, int height)
		{
			// Reset cloud points.
			// CloudPoints = new List<PointF>(0);
			
			var titleFont = new Font(FontFamily.GenericSerif, 36);
			var point = PointF.Empty;

			// If time, move strings to file.
			var mainTitle = "Don Quixote";
			point.X = width / 2 - (Renderer.Graphics.MeasureString(mainTitle, titleFont).Width / 2);
			point.Y = height / 3 - (titleFont.Height / 2);
			Renderer.AddText(mainTitle, titleFont, Brushes.White, point);

			// Create cloud points.
			// PointF leftCloud = PointF.Empty;
			// leftCloud.X = point.X / 2 - CLOUD_SIZE / 2;
			// leftCloud.Y = point.Y;

			// PointF rightCloud = PointF.Empty;
			// rightCloud.X = (height - (point.X + Renderer.Graphics.MeasureString(mainTitle, titleFont).Width)) / 2 - CLOUD_SIZE;
			// rightCloud.Y = point.Y;

			// CloudPoints.Add(leftCloud);
			// CloudPoints.Add(rightCloud);

			point.Y += titleFont.Height;
			titleFont = new Font(FontFamily.GenericSerif, 20);

			var secondaryTitle = "de la Mancha";
			point.X = CenterString(secondaryTitle, titleFont, width);
			Renderer.AddText(secondaryTitle, titleFont, Brushes.White, point);

			titleFont = new Font(FontFamily.GenericSerif, 36);

			var prompt = "Press I to view instructions";
			point.X = CenterString(prompt, titleFont, width);
			point.Y = height - ((titleFont.Height * 1.1f) * 2);
			Renderer.AddText(prompt, titleFont, Brushes.White, point);

			prompt = "Press space to begin";
			point.X = CenterString(prompt, titleFont, width);
			point.Y += titleFont.Height * 1.1f;
			Renderer.AddText(prompt, titleFont, Brushes.White, point);
		}

		public void AddInstructionsStrings(int width, int height)
		{
			var instructionsFont = new Font(FontFamily.GenericSerif, 36);
			var point = PointF.Empty;

			var title = "Instructions";
			point.X = CenterString(title, instructionsFont, width);
			Renderer.AddText(title, instructionsFont, Brushes.White, point);

			point.Y += instructionsFont.Height * 2.0f;
			instructionsFont = new Font(FontFamily.GenericSerif, 32);

			var moveText = "W => Move Up\nA => Move Left\nS => Move Down\nD => Move Right";
			point.X = PointF.Empty.X;
			Renderer.AddText(moveText, instructionsFont, Brushes.White, point);
			point.Y += instructionsFont.Height * 4.0f;

			var rotateText = "Don Quixote will face towards the mouse.\nMove the mouse to adjust where he is facing.";
			point.Y += instructionsFont.Height;
			Renderer.AddText(rotateText, instructionsFont, Brushes.White, point);

			var backText = "Press M at any time to go back to the menu.";
			point.X = CenterString(backText, instructionsFont, width);
			point.Y = height * 0.85f;
			Renderer.AddText(backText, instructionsFont, Brushes.White, point);
		}

		public float CenterString(string text, Font font, int screenWidth)
		{ 
			return screenWidth / 2 - (Renderer.Graphics.MeasureString(text, font).Width / 2);
		}
		
		public void Render(Mode mode, Manager manager)
		{
			GL.Clear(ClearBufferMask.ColorBufferBit);

			if (mode == Mode.Menu) {
				GL.ClearColor(Color.SkyBlue);

				// foreach (PointF cloudPoint in CloudPoints)
				// 	Cloud.RenderWidthHeight(cloudPoint.X, cloudPoint.Y, CLOUD_SIZE, CLOUD_SIZE);
				Cloud.RenderWidthHeight(-0.7f, 0.15f, CLOUD_SIZE, CLOUD_SIZE);
				Cloud.RenderWidthHeight(0.7f - CLOUD_SIZE, 0.15f, CLOUD_SIZE, CLOUD_SIZE);
			} else if (mode == Mode.Instructions) {
				GL.ClearColor(Color.Black);
			} else if (mode == Mode.InGame) {
				GL.ClearColor(Color.Green);

				manager.RenderEnvironment();
				manager.RenderCharacters();
			}

			Renderer.Render();
		}
	}
}
