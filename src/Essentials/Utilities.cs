using System;
using OpenTK;

namespace Essentials
{
	public class Utilities
	{
		// A class containing utilities relating to X coordinates.
		public class X
		{
			// Converts a drawing float into an integer.
			public static int FloatToInt(float coordinate, int screenWidth)
			{
				return (int)(screenWidth * ((coordinate / 2) + 0.5f));
			}

			// Converts an integer into a drawing float.
			public static float IntToFloat(int coordinate, int screenWidth)
			{
				return 0.0f; // ADD METHOD
			}
		}

		// A class containing utilities relating to Y coordinates.
		public class Y
		{
			// Converts a drawing float into an integer.
			public static int FloatToInt(float coordinate, int screenHeight)
			{
				return (int)(screenHeight * (1.0f - ((coordinate / 2) + 0.5f)));
			}

			// Converts an integer into a drawing float.
			public static float IntToFloat(int coordinate, int screenHeight)
			{
				return 0.0f; // ADD METHOD
			}
		}

		// Returns the calculated angle between the two in degrees.
		public static double CalculateAngleDegrees(double y, double x)
		{
			return MathHelper.RadiansToDegrees(Math.Atan2(-y, -x)) + 180.0;
		}
	}
}
