using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Input;
using Characters;
using Environment;

namespace Essentials
{
	public class Manager
	{
		private const float MOVEMENT_RATE = 0.017f;

		private Random random;

		private List<Windmill> windmills;
		private int windmillNumber = 3;
		private int windmillsDestroyed = 0;
		
		private DonQuixote player;

		private Random Random {
			get {
				return random;
			}
			set {
				random = value;
			}
		}

		private DonQuixote Player {
			get {
				return player;
			}
			set {
				player = value;
			}
		}

		private List<Windmill> Windmills {
			get {
				return windmills;
			}
			set {
				windmills = value;
			}
		}

		public int WindmillsDestroyed {
			get {
				return windmillsDestroyed;
			}
			set {
				windmillsDestroyed = value;
			}
		}

		public Manager()
		{
			this.Random = new Random();
			
			Player = new DonQuixote();

			Windmills = new List<Windmill>(0);

			for (int i = 0; i < windmillNumber; i++) {
				do {
					this.Windmills.Add(new Windmill(Random.Next() % 2 == 0 ? (float)(-(Random.NextDouble() * 0.8)) : (float)(Random.NextDouble() * 0.8), Random.Next() % 2 == 0 ? (float)(-(Random.NextDouble() * 0.8)) : (float)(Random.NextDouble() * 0.8)));
				} while (Player.Rectangle.Intersects(Windmills[i].Rectangle));
			}
		}

		public void RenderEnvironment()
		{
			foreach (Windmill windmill in Windmills) {
				windmill.Render();
			}
		}

		public void RenderCharacters()
		{
			Player.Render();
		}

		/*
		 * Handles keyboard presses.
		 *
		 * Argument keyboard is a KeyboardDevice.
		 * This is an OpenTK class in OpenTK.Input that allows indexers.
		 */
		public void KeyboardHandle(KeyboardDevice keyboard)
		{
			// Do not use `else if` to allow for multiple keys down.
			// Check for collisions on movement.
			// TODO Switch tree to circle.
			if (keyboard[Key.W]) {
				Player.Y += MOVEMENT_RATE;

				foreach (Windmill windmill in Windmills)
					if (Player.Rectangle.Intersects(windmill.Rectangle))
						Player.Y -= MOVEMENT_RATE * 2.0f;
			}
			if (keyboard[Key.A]) {
				Player.X -= MOVEMENT_RATE;

				foreach (Windmill windmill in Windmills)
					if (Player.Rectangle.Intersects(windmill.Rectangle))
						Player.X += MOVEMENT_RATE * 2.0f;
			}
			if (keyboard[Key.S]) {
				Player.Y -= MOVEMENT_RATE;

				foreach (Windmill windmill in Windmills)
					if (Player.Rectangle.Intersects(windmill.Rectangle))
						Player.Y += MOVEMENT_RATE * 2.0f;
			}
			if (keyboard[Key.D]) {
				Player.X += MOVEMENT_RATE;

				foreach (Windmill windmill in Windmills)
					if (Player.Rectangle.Intersects(windmill.Rectangle))
						Player.X -= MOVEMENT_RATE * 2.0f;
			}
		}

		/*
		 * Handles mouse activity.
		 *
		 * Argument mouse is a MouseDevice.
		 * This is an OpenTK class in OpenTK.Input that allows indexers.
		 */
		public void MouseHandle(MouseDevice mouse, int width, int height)
		{
			var playerIntX = Utilities.X.FloatToInt(Player.X, width);
			var playerIntY = Utilities.Y.FloatToInt(Player.Y, height);

			/*
			 * Sets the correct rotation for the player to be facing the mouse.
			 * The Y is negated because it was flipped for some reason.
			 * Set to nearest 90 because I cannot do trigonometry.
			 */
			Player.Rotation = (float)Utilities.CalculateAngleDegrees(-(mouse.Y - playerIntY), mouse.X - playerIntX);

			if (Player.Rotation >= 45.0f && Player.Rotation < 135.0f)
				Player.Rotation = 90.0f;
			else if (Player.Rotation >= 135.0f && Player.Rotation < 225.0f)
				Player.Rotation = 180.0f;
			else if (Player.Rotation >= 225.0f && Player.Rotation < 315.0f)
				Player.Rotation = 270.0f;
			else
				Player.Rotation = 0.0f;

			// Extends player's sword if left mouse down.
			Player.SwordExtended = mouse[MouseButton.Left];
		}

		// Handles CPU logic.
		public void Logic()
		{
			foreach (Windmill windmill in Windmills) {
				if (Player.SwordRectangle.Intersects(windmill.Rectangle)) {
					WindmillsDestroyed++;
					
					do {
						windmill.X = Random.Next() % 2 == 0 ? (float)(-(Random.NextDouble() * 0.8)) : (float)(Random.NextDouble() * 0.8);
						windmill.Y = Random.Next() % 2 == 0 ? (float)(-(Random.NextDouble() * 0.8)) : (float)(Random.NextDouble() * 0.8);
					} while (Player.Rectangle.Intersects(windmill.Rectangle));
				}
			}
		}
	}
}
