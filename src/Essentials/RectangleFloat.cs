/*
 * A rectangle class made to specify all four corner points.
 * C#'s native RectangleF could not provide this.
 */

using System.Drawing;
using System.Collections.Generic;

namespace Essentials
{
	public class RectangleFloat
	{
		private float x1;
		private float y1;
		private float x2;
		private float y2;

		public float X1 {
			get {
				return x1;
			}
			set {
				x1 = value;
			}
		}

		public float Y1 {
			get {
				return y1;
			}
			set {
				y1 = value;
			}
		}

		public float X2 {
			get {
				return x2;
			}
			set {
				x2 = value;
			}
		}

		public float Y2 {
			get {
				return y2;
			}
			set {
				y2 = value;
			}
		}

		public RectangleFloat(float x1, float y1, float x2, float y2)
		{
			X1 = x1;
			Y1 = y1;
			X2 = x2;
			Y2 = y2;
		}

		public RectangleFloat(PointF from, PointF to) : this(from.X, from.Y, to.X, to.Y)
		{
			
		}

		public List<PointF> Intersection(RectangleFloat otherRectangle)
		{
			var result = new List<PointF>(0);

			var lines = new LineF[4] {new LineF(X1, Y1, X2, Y1), new LineF(X2, Y1, X2, Y2), new LineF(X2, Y2, X1, Y2), new LineF(X1, Y2, X1, Y1)};

			foreach (LineF line in lines) {
				AddIfIntersect(line, otherRectangle.X1, otherRectangle.Y1, otherRectangle.X2, otherRectangle.Y1, result);
				AddIfIntersect(line, otherRectangle.X2, otherRectangle.Y1, otherRectangle.X2, otherRectangle.Y2, result);
				AddIfIntersect(line, otherRectangle.X2, otherRectangle.Y2, otherRectangle.X1, otherRectangle.Y2, result);
				AddIfIntersect(line, otherRectangle.X1, otherRectangle.Y2, otherRectangle.X1, otherRectangle.Y1, result);
			}

			return result;
		}

		public List<PointF> Intersection(LineF line)
		{
			var result = new List<PointF>(0);
			
			AddIfIntersect(line, X1, Y1, X2, Y1, result);
			AddIfIntersect(line, X2, Y1, X2, Y2, result);
			AddIfIntersect(line, X2, Y2, X1, Y2, result);
			AddIfIntersect(line, X1, Y2, X1, Y1, result);
			
			return result;
		}

		public bool Intersects(RectangleFloat otherRectangle)
		{
			return Intersection(otherRectangle).Count > 0;
		}

		public bool Intersects(LineF line)
		{
			return Intersection(line).Count > 0;
		}

		private void AddIfIntersect(LineF line, float x1, float y1, float x2, float y2, ICollection<PointF> result)
		{
			var l2 = new LineF(x1, y1, x2, y2);
			var intersection = line.Intersection(l2);
			
			if (intersection != null)
				result.Add(intersection.Value);
		}

		public PointF FromCorner {
			get {
				return new PointF(X1, Y1);
			}
		}

		public PointF ToCorner {
			get {
				return new PointF(X2, Y2);
			}
		}
	}
}
