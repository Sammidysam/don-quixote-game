/*
 * Thanks to Joost van Schaik for the code!
 * http://wp7nl.codeplex.com/SourceControl/latest#Wp7nl/Wp7nl/Utilities/LineF.cs
 *
 * The code has been touched up to the present coding style, but the algorithms present are by the above person.
 */

using System;
using System.Drawing;
using System.Collections.Generic;

namespace Essentials
{
	public class LineF
	{
		private float x1;
		private float y1;
		private float x2;
		private float y2;
		
		public float X1 {
			get {
				return x1;
			}
			set {
				x1 = value;
			}
		}
		
		public float Y1 {
			get {
				return y1;
			}
			set {
				y1 = value;
			}
		}

		public float X2 {
			get {
				return x2;
			}
			set {
				x2 = value;
			}
		}

		public float Y2 {
			get {
				return y2;
			}
			set {
				y2 = value;
			}
		}

		public LineF(float x1, float y1, float x2, float y2)
		{
			X1 = x1;
			Y1 = y1;
			X2 = x2;
			Y2 = y2;
		}

		public LineF(PointF from, PointF to) : this(from.X, from.Y, to.X, to.Y)
		{
			
		}
		
		public PointF? Intersection(LineF otherLine)
		{
			var a1 = Y2 - Y1;
			var b1 = X1 - X2;
			var c1 = X2 * Y1 - X1 * Y2;

			// Compute r3 and r4.

			var r3 = a1 * otherLine.X1 + b1 * otherLine.Y1 + c1;
			var r4 = a1 * otherLine.X2 + b1 * otherLine.Y2 + c1;

			/*
			 * Check signs of r3 and r4.  If both point 3 and point 4 lie on
			 * same side of line 1, the line segments do not intersect.
			 */

			if (r3 != 0 && r4 != 0 && Math.Sign(r3) == Math.Sign(r4)) {
				return null; // DONT_INTERSECT
			}

			// Compute a2, b2, c2

			var a2 = otherLine.Y2 - otherLine.Y1;
			var b2 = otherLine.X1 - otherLine.X2;
			var c2 = otherLine.X2 * otherLine.Y1 - otherLine.X1 * otherLine.Y2;

			// Compute r1 and r2

			var r1 = a2 * X1 + b2 * Y1 + c2;
			var r2 = a2 * X2 + b2 * Y2 + c2;

			/*
			 * Check signs of r1 and r2.  If both point 1 and point 2 lie
			 * on same side of second line segment, the line segments do
			 * not intersect.
			 */
			if (r1 != 0 && r2 != 0 && Math.Sign(r1) == Math.Sign(r2)) {
				return (null); // DONT_INTERSECT
			}

			// Line segments intersect: compute intersection point.

			var denom = a1 * b2 - a2 * b1;
			if (denom == 0)
				return null; //( COLLINEAR );
			
			var offset = denom < 0 ? -denom / 2 : denom / 2;

			/*
			 * The denom/2 is to get rounding instead of truncating.  It
			 * is added or subtracted to the numerator, depending upon the
			 * sign of the numerator.
			 */

			var num = b1 * c2 - b2 * c1;
			var x = (num < 0 ? num - offset : num + offset) / denom;

			num = a2 * c1 - a1 * c2;
			var y = (num < 0 ? num - offset : num + offset) / denom;

			return new PointF(x, y);
		}
		
		public List<PointF> Intersection(RectangleFloat rectangle)
		{
			var result = new List<PointF>(0);
			
			AddIfIntersect(rectangle.X1, rectangle.Y1, rectangle.X2, rectangle.Y1, result);
			AddIfIntersect(rectangle.X2, rectangle.Y1, rectangle.X2, rectangle.Y2, result);
			AddIfIntersect(rectangle.X2, rectangle.Y2, rectangle.X1, rectangle.Y2, result);
			AddIfIntersect(rectangle.X1, rectangle.Y2, rectangle.X1, rectangle.Y1, result);
			
			return result;
		}

		public bool Intersects(LineF otherLine)
		{
			return Intersection(otherLine) != null;
		}

		public bool Intersects(RectangleFloat rectangle)
		{
			return Intersection(rectangle).Count > 0;
		}

		private void AddIfIntersect(float x1, float y1, float x2, float y2, ICollection<PointF> result)
		{
			var l2 = new LineF(x1, y1, x2, y2);
			var intersection = Intersection(l2);
			
			if (intersection != null)
				result.Add(intersection.Value);
		}

		public PointF From {
			get {
				return new PointF(X1, Y1);
			}
		}

		public PointF To {
			get {
				return new PointF(X2, Y2);
			}
		}
	}
}
