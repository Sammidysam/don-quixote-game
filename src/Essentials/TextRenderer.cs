using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Platform;

namespace Essentials
{
	public class TextRenderer : IDisposable
	{
		Bitmap bmp;
		Graphics gfx;
		int texture;
		Rectangle dirty_region;
		bool disposed;

		List<string> strings;
		List<Font> fonts;
		List<Brush> brushes;
		List<PointF> points;

		public Graphics Graphics {
			get {
				return gfx;
			}
		}
			
		public TextRenderer(int width, int height)
		{
			if (width <= 0)
				throw new ArgumentOutOfRangeException("width");
			if (height <= 0)
				throw new ArgumentOutOfRangeException("height");
			if (GraphicsContext.CurrentContext == null)
				throw new InvalidOperationException("No GraphicsContext is current on the calling thread.");

			bmp = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			gfx = Graphics.FromImage(bmp);
			gfx.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

			texture = GL.GenTexture();
			Console.WriteLine(texture + " " + bmp);
			
			GL.BindTexture(TextureTarget.Texture2D, texture);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0, PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero);

			strings = new List<string>(0);
			fonts = new List<Font>(0);
			brushes = new List<Brush>(0);
			points = new List<PointF>(0);
		}
			
		public void Clear()
		{
			// Clear with transparency as to not overlay a color over the screen.
			gfx.Clear(Color.Transparent);
			dirty_region = new Rectangle(0, 0, bmp.Width, bmp.Height);
		}
			
		private void DrawString(string text, Font font, Brush brush, PointF point)
		{
			gfx.DrawString(text, font, brush, point);

			SizeF size = gfx.MeasureString(text, font);
			dirty_region = Rectangle.Round(RectangleF.Union(dirty_region, new RectangleF(point, size)));
			dirty_region = Rectangle.Intersect(dirty_region, new Rectangle(0, 0, bmp.Width, bmp.Height));
		}

		public void Render()
		{
			GL.Color3(Color.White);
			GL.Enable(EnableCap.Texture2D);
			GL.BindTexture(TextureTarget.Texture2D, Texture);

			GL.Begin(BeginMode.Quads);
			GL.TexCoord2(0.0f, 1.0f);
			GL.Vertex2(-1.0f, -1.0f);
			GL.TexCoord2(1.0f, 1.0f);
			GL.Vertex2(1.0f, -1.0f);
			GL.TexCoord2(1.0f, 0.0f);
			GL.Vertex2(1.0f, 1.0f);
			GL.TexCoord2(0.0f, 0.0f);
			GL.Vertex2(-1.0f, 1.0f);
			GL.End();

			GL.Disable(EnableCap.Texture2D);
		}

		public void AddText(string text, Font font, Brush brush, PointF location)
		{
			strings.Add(text);
			fonts.Add(font);
			brushes.Add(brush);
			points.Add(location);

			DrawString(text, font, brush, location);
		}

		public void RemoveText(string text)
		{
			// To remove text, we have to remove one string then add all the others again. 
			for (int i = 0; i < strings.Count; i++) {
				if (strings[i] == text) {
					strings.RemoveAt(i);
					fonts.RemoveAt(i);
					brushes.RemoveAt(i);
					points.RemoveAt(i);
				}
			}

			Clear();

			for (int i = 0; i < strings.Count; i++) {
				DrawString(strings[i], fonts[i], brushes[i], points[i]);
			}
		}
			
		public int Texture
		{
			get {
				UploadBitmap();
				
				return texture;
			}
		}
			
		void UploadBitmap()
		{
			if (dirty_region != RectangleF.Empty)
			{
				System.Drawing.Imaging.BitmapData data = bmp.LockBits(dirty_region, System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

				GL.BindTexture(TextureTarget.Texture2D, texture);
				GL.TexSubImage2D(TextureTarget.Texture2D, 0, dirty_region.X, dirty_region.Y, dirty_region.Width, dirty_region.Height, PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);

				bmp.UnlockBits(data);

				dirty_region = Rectangle.Empty;
			}
		}

		void Dispose(bool manual)
		{
			if (!disposed) {
				if (manual) {
					bmp.Dispose();
					gfx.Dispose();
					if (GraphicsContext.CurrentContext != null)
						GL.DeleteTexture(texture);
				}

				disposed = true;
			}
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
