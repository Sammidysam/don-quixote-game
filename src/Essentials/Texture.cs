using System;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK.Graphics.OpenGL;

namespace Essentials {
	public class Texture {
		private int id;

		private int width;
		private int height;

		private float rotation;

		private string filename;

		public int ID {
			get {
				return id;
			}
			private set {
				id = value;
			}
		}

		public int Width {
			get {
				return width;
			}
			private set {
				width = value;
			}
		}

		public int Height {
			get {
				return height;
			}
			private set {
				height = value;
			}
		}

		public float Rotation {
			get {
				return rotation;
			}
			set {
				if (value >= 0.0f && value < 360.0f)
					rotation = value;
				else
					Console.WriteLine("Invalid rotation angle " + value);
			}
		}

		public string Filename {
			get {
				return filename;
			}
			set {
				filename = value;
			}
		}
		
		public Texture(string filename)
		{
    		if(String.IsNullOrEmpty(filename))
        		throw new ArgumentException(filename);

			Filename = filename;
			
    		int id = GL.GenTexture();
    		Console.WriteLine(id + " " + filename);

			// texture binding stuff
    		GL.BindTexture(TextureTarget.Texture2D, id);
    		Bitmap bmp = new Bitmap(filename);
    		BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
    		GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bmpData.Width, bmpData.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bmpData.Scan0);

			// Set texture width and height.
			Width = bmpData.Width;
			Height = bmpData.Height;
			
    		bmp.UnlockBits(bmpData);
    		GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
   			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

			// Set ID.
			ID = id;
		}

		private void PrepareGL()
		{
			GL.Color3(Color.White);
			GL.Enable(EnableCap.Texture2D);
			GL.BindTexture(TextureTarget.Texture2D, ID);
		}

		private void EndGL()
		{
			GL.Disable(EnableCap.Texture2D);
		}

		public void RenderEndpoints(float x, float y, float xEnd, float yEnd)
		{
			PrepareGL();
			
			GL.Begin(BeginMode.Quads);
			GL.TexCoord2(0.0, 1.0);
			GL.Vertex2(x, y);
			GL.TexCoord2(0.0, 0.0);
			GL.Vertex2(x, yEnd);
			GL.TexCoord2(1.0, 0.0);
			GL.Vertex2(xEnd, yEnd);
			GL.TexCoord2(1.0, 1.0);
			GL.Vertex2(xEnd, y);
			GL.End();

			EndGL();
		}
		
		public void RenderWidthHeight(float x, float y, float width, float height)
		{
			PrepareGL();

			GL.PushMatrix();
			
			GL.Translate(x + (width / 2), y + (height / 2), 0);
			GL.Rotate(Rotation, 0, 0, 1);
			GL.Translate(-(x + (width / 2)), -(y + (height / 2)), 0);

			GL.Begin(BeginMode.Quads);
			GL.TexCoord2(0.0, 1.0);
			GL.Vertex2(x, y);
			GL.TexCoord2(0.0, 0.0);
			GL.Vertex2(x, y + height);
			GL.TexCoord2(1.0, 0.0);
			GL.Vertex2(x + width, y + height);
			GL.TexCoord2(1.0, 1.0);
			GL.Vertex2(x + width, y);
			GL.End();

			GL.PopMatrix();

			EndGL();
		}
	}
}
